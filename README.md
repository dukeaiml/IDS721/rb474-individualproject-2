# rb474-individualproject-2

The project is a small containerised web app which returns a `base` in power `exponent` if the values are provided in the HTTP request. A CI/CD action is triggered on push which tries to build the image to indicate if there is any problem with it.

Few notes:
- The values of `base` and `exponent` are to be unsigned integers.
- The port is 80.
- To run the app use:
```
make build
make run
```
Then go to your browser and access the app [locally](http://localhost:80?base=3&exponent=3).

The video documentation can be found [here](./media/).