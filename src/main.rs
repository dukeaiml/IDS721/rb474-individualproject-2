use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;

#[derive(Deserialize)]
struct PowerSomething {
    base: Option<u32>,
    exponent: Option<u32>,
}

async fn add(arguments: web::Query<PowerSomething>) -> impl Responder {
    match (arguments.base, arguments.exponent) {
        (Some(base), Some(exponent)) => {
            let result = base.pow(exponent);
            HttpResponse::Ok().body(format!("{} power {} is {}", base, exponent, result))
        },
        _ => HttpResponse::BadRequest().body("Wrong arguments!"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/", web::get().to(add)))
    .bind("0.0.0.0:80")?
    .run()
    .await
}
