FROM rust:1.77 as builder

WORKDIR /app

COPY . .

RUN cargo build --release

FROM debian:bookworm-slim
COPY --from=builder /app/target/release/actix .

CMD ["./actix"]
